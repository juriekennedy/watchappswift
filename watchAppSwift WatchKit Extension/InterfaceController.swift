//
//  InterfaceController.swift
//  watchAppSwift WatchKit Extension
//
//  Created by Jurie Kennedy on 2/2/20.
//  Copyright © 2020 Jurie Kennedy. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    @IBOutlet weak var group: WKInterfaceGroup!
    @IBOutlet weak var canvas: WKInterfaceImage!
    var bounds:CGRect!
    var coordinateSystem:CoordinateSystem!
    var image:UIImage!
    
    @IBAction func onRecognizerStateChange(_ recognizer: WKTapGestureRecognizer) {
        switch recognizer.state {
        case .began:
            print("User just touched the screen, finger is still resting")
            coordinateSystem.setBounds(bounds: recognizer.objectBounds())
            self.bounds = coordinateSystem.getBounds()
            drawTouchBegan(recognizer)
        case .ended:
            print("User has raised his finger")
        case .changed:
            drawTouchMoved(recognizer)
        case .recognized:
            print("User finger recognized")
        default:
            print("Other touch state")
            break
        }
    }
    

    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.setTitle("")
        print("----RUNNING----");
        let currentDevice = WKInterfaceDevice.current()
        bounds = currentDevice.screenBounds
        coordinateSystem = CoordinateSystem.init(bounds: bounds)
        //CoordinateSystem.setShared(share: coordinateSystem)
        //coordinateSystem = CoordinateSystem.getInstance(bounds: bounds)
        
        image = UIImage(named: "nb_list_icon")
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //example drawing (cross)
    func drawTouchBegan(_ recognizer: WKTapGestureRecognizer) {
        let context = Drawing.startScreenDrawing(screenSize: coordinateSystem.getBounds().size)
        Drawing.drawCross( coordinateSystem: coordinateSystem)
        canvas.setImage(Drawing.getScreenDrawing(context: context))
        Drawing.endScreenDrawing()
    }
    
    func drawTouchMoved(_ recognizer: WKTapGestureRecognizer) {
        let context = Drawing.startScreenDrawing(screenSize: coordinateSystem.getBounds().size)
        
        let normalizedPoint = self.coordinateSystem.canvasToNormalized(point: CGPoint(x: recognizer.locationInObject().x, y: recognizer.locationInObject().y))
        
        //drow touch circle
        //Drawing.drawCircle(radius: 0.25, centerPoint: normalizedPoint, coordinateSystem: coordinateSystem)
        
        //drow touch hamburger icon
        Drawing.drawImage(image: "nb_list_icon", radius: 0.25, centerPoint: normalizedPoint, coordinateSystem: coordinateSystem)
        
        drawBubbles()
        
        canvas.setImage(Drawing.getScreenDrawing(context: context))
        Drawing.endScreenDrawing()
    }
    
    func drawResting(_ recognizer: WKTapGestureRecognizer) {
        let context = Drawing.startScreenDrawing(screenSize: coordinateSystem.getBounds().size)
        
        let normalizedPoint = self.coordinateSystem.canvasToNormalized(point: CGPoint(x: recognizer.locationInObject().x, y: recognizer.locationInObject().y))
        
        Drawing.drawCircle(radius: 0.25, centerPoint: normalizedPoint, coordinateSystem: coordinateSystem)
        canvas.setImage(Drawing.getScreenDrawing(context: context))
        Drawing.endScreenDrawing()
    }
    
    //draw a few bubbles around screen center
    func drawBubbles() {
        let numberOfCircles = 6
        let bigRadius = 0.75
        
        for index in 1...numberOfCircles {
            let angle = Double(index) * (2.0 * Double.pi)/Double(numberOfCircles)
            let centerPoint = CGPoint(x: bigRadius * cos(angle), y: bigRadius * sin(angle))
            
            Drawing.drawCircle(radius: 0.20, centerPoint: centerPoint, coordinateSystem: coordinateSystem)
        }
    }
}
