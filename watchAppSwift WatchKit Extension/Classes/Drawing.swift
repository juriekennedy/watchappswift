//
//  Drawing.swift
//  watchAppSwift
//
//  Created by Jurie Kennedy 
//  Copyright © 2020 Jurie Kennedy. All rights reserved.
//

import WatchKit
import Foundation

class Drawing {
    
    /**
    Always call startScreenDrawing first
    Then do all drawing (eg: multiple drawCircle / drawImage).
    Then get the canvas image with getScreenDrawing
    And always end it with endScreenDrawing
     */
    
    static func startScreenDrawing(screenSize: CGSize) -> CGContext {
        let size = CGSize(width:screenSize.width, height:  screenSize.height)
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        UIGraphicsPushContext(context!)
        return context!
    }
    
    //draw a white circle at coordiate with radius
    //center of screen is 0,0 with screen width = 1
    static func getScreenDrawing(context: CGContext?) -> UIImage {
        // Convert to UIImage
        let cgimage = context?.makeImage()
        let uiimage = UIImage.init(cgImage: cgimage!)
        return uiimage
    }
    
    static func endScreenDrawing() {
        // End the graphics context
        UIGraphicsPopContext()
        UIGraphicsEndImageContext()
    }
    
    //draw a white circle at coordiate with radius
    //center of screen is 0,0 with screen width = 1
    static func drawCircle(radius: CGFloat, centerPoint: CGPoint, coordinateSystem: CoordinateSystem) {
        
        // Setup for the path appearance
        UIColor.green.setStroke()
        UIColor.white.setFill()

        var canvasPoint = coordinateSystem.normalizedToCanvas(point: centerPoint)
        let canvasRadius = coordinateSystem.canvasUnit(length: radius)
        
        //deduct radius so cicle is centered around center point
        canvasPoint = CGPoint(x: canvasPoint.x - canvasRadius, y: canvasPoint.y - canvasRadius)
        // Draw circle
        let rect = CGRect(origin:canvasPoint, size: CGSize(width: canvasRadius * 2, height: canvasRadius * 2))
        let path = UIBezierPath(ovalIn: rect)
        //let path = UIBezierPath(roundedRect: rect, cornerRadius: canvasRadius)
        path.lineWidth = 2.0
        path.fill()
        path.stroke()
    }
    
    //draw a fullscreen white cross (just a proof of concept)
    static func drawCross(coordinateSystem: CoordinateSystem) {
        
        let path = UIBezierPath()
        // Setup for the path appearance
        path.lineWidth = 4.0
        UIColor.white.setStroke()
        
        //add cross to path
        path.move(to: CGPoint(x:0, y:0))
        path.addLine(to: CGPoint(x: coordinateSystem.getBounds().size.width, y: coordinateSystem.getBounds().size.height))
        path.move(to: CGPoint(x:0, y: coordinateSystem.getBounds().size.height))
        path.addLine(to: CGPoint(x: coordinateSystem.getBounds().size.width, y:0))
        path.stroke()
    }
    
    //draw a fullscreen white cross (just a proof of concept)
    static func drawImage(image: String, radius: CGFloat, centerPoint: CGPoint, coordinateSystem: CoordinateSystem) {
        
        let icon = UIImage(named: image)
        var canvasPoint = coordinateSystem.normalizedToCanvas(point: centerPoint)
        let canvasRadius = coordinateSystem.canvasUnit(length: radius)
        
        //deduct radius so cicle is centered around center point
        canvasPoint = CGPoint(x: canvasPoint.x - canvasRadius, y: canvasPoint.y - canvasRadius)
        // Draw circle
        let rect = CGRect(origin:canvasPoint, size: CGSize(width: canvasRadius * 2, height: canvasRadius * 2))
        
        icon?.draw(in: rect)
    }
    
}
