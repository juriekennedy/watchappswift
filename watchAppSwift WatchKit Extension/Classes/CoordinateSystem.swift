//
//  CoordinateSystem.swift
//  watchAppSwift WatchKit App
//
//  Created by Jurie Kennedy
//  Copyright © 2020 Jurie Kennedy. All rights reserved.
//
import WatchKit
import Foundation

class CoordinateSystem {
    private var bounds:CGRect
    static var shared:CoordinateSystem = CoordinateSystem.init(bounds: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 0, height: 0)))
    
    static func getShared() -> CoordinateSystem {
        return shared
    }
    
    static func setShared(share:CoordinateSystem ) {
        shared = share
    }
    
    static func getInstance(bounds: CGRect) -> CoordinateSystem {
        return shared
    }
    
    
    init(bounds: CGRect) {
        self.bounds = bounds
    }
    
    //take canvas coords and transfor to normalizes (where 0,0 is center of screen and 1,1 is top right, etc.)
    func canvasToNormalized(point: CGPoint) -> CGPoint {
        return CGPoint(x: (point.x - self.bounds.size.width/2)/(self.bounds.size.width/2), y: (self.bounds.size.height/2 - point.y)/(self.bounds.size.height/2))
    }
    
    //take normalized coords and transfor to canvas
    func normalizedToCanvas(point: CGPoint) -> CGPoint {
        return CGPoint(x: self.bounds.size.width/2 + point.x * self.bounds.size.width/2, y: self.bounds.size.height/2 - point.y * self.bounds.size.height/2)
    }
    
    //take normalized coords and transfor to canvas
    func canvasUnit(length: CGFloat) -> CGFloat {
        return length * self.bounds.size.width/2
    }
    
    //take normalized coords and transfor to canvas
    func normalizedUnit(length: CGFloat) -> CGFloat {
        return length / self.bounds.size.width/2
    }
    
    func setBounds(bounds: CGRect) {
        self.bounds = bounds
    }
    
    func getBounds() -> CGRect {
        return self.bounds
    }
}
